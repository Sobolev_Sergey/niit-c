/* Компрессор по методу Хаффмана

1. Упаковка файла:
c:\compres.exe -c [имя_файла].[расширение]
c:\compres.exe -c war_peace.txt

2. Распаковка файла:
c:\compres.exe -d [имя_архива].[расширение]
c:\compres.exe -d war_peace.txt.arh

*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "struct.h"

int main(int argc,char *argv[])
{
	int unique_symbol=0;	//счётчик количества различных букв, уникальных символов
	int count_us=0;			//счётчик количества всех знаков в файле
    int count_01=0;			//счётчик количества символов из 0 и 1 в промежуточном файле temp
	int tail_length;		//размер хвоста файла (то, что не кратно 8 в промежуточном файле)
    int arr_us[256]={0};	//инициализируем массив количества уникальных символов
	SYM simbols[256]={0};	//инициализируем массив записей 
	SYM *psym[256];			//инициализируем массив указателей на записи
    int mes[8];				//массив 0 и 1
   	char file_name_input[256]={0};					//массив под имя входящего файла
	char file_mame_101[256]={0};					//массив под имя бинарного промежуточного файла
	char file_mame_arh[256]={0};					//массив под имя созданого архива
	FILE *fp_input,*fp_101,*fp_result;				//указатели на файлы


	if(argc < 2) //проверяем количество входных аргументов, должно быть 2 (-с или -d и имя файла)
	{
		printf("No files in arguments!\n");
		return -1;
	}

	if(strcmp(argv[1],"-c")==0) //запускаем компрессор
	{
		strcpy(file_name_input,argv[2]);
		fp_input=open_file(file_name_input,"rb");
		//fp_input=open_file("war_peace.txt","rb");		//открываем конкретный файл
		printf("Name imput file: %s\n",file_name_input);
		//fp_101=open_file("temp.101","wb");				//промежуточный бинарный файл
		fp_101=open_file(add_file(file_name_input,file_mame_101,".101"),"wb");
		//fp_result=open_file("war_peace.txt.arh","wb");	//открываем файл для записи сжатого файла
		fp_result=open_file(add_file(file_name_input,file_mame_arh,".arh"),"wb");
		
		count_us=read_file(fp_input,simbols,arr_us,&unique_symbol);//побайтно читаем файл и составляем таблицу встречаемости
		calculating_frequency(unique_symbol,simbols,arr_us,count_us);// Расчёт частоты встречаемости
		records_addresses(unique_symbol,psym,simbols);//в массив указателей заносим адреса записей
		descending_sort(unique_symbol,simbols); //сортировка по убыванию
		print_char_frec(unique_symbol,count_us,simbols);// вывод на экран таблицы символов с частотой по убыванию
		
		SYM *root=buildTree(psym,unique_symbol);//вызов функции создания дерева Хаффмана
		makeCodes(root);//вызов функции получения кода
		
		//в цикле читаем исходный файл, и записываем полученные в функциях коды в промежуточный файл
		write_file_fp_101(fp_input,unique_symbol,simbols,fp_101);
		
		fp_101=open_file(add_file(file_name_input,file_mame_101,".101"),"rb");
		count_01=size_binary_file(fp_101);//Считаем размер бинарного файла(количество символов в нём)
		
		// вывод на экран таблицы символов с новыми кодами и количеством символов в бинарном файле
		print_count_unique_symbol(unique_symbol,simbols,count_01);
		
		tail_length=count_01%8;//находим остаток, количество символов не кратных 8 (хвост)
		
		//формируем заголовок сжатого файла через поля байтов
		fwrite("Compresing!!!",sizeof(char),13,fp_result);		//условная подпись
		fwrite(&unique_symbol,sizeof(int),1,fp_result);			//количество уникальных символов
		fwrite(&tail_length,sizeof(int),1,fp_result);			//величина хвоста
		
		//Записываем в сжатый файл таблицу встречаемости
		write_table_occurrence(unique_symbol,simbols,fp_result);
		
		rewind(fp_101);//возвращаем указатель в промежуточном файле в начало файла
		function_packing(count_01,tail_length,mes,fp_101,code,fp_result);//Функция упаковки
		function_tail_length(tail_length,mes,fp_101,code,fp_result);//Функция длины хвоста
		
		printf("File \"%s\" pack to \"%s\"!\n",file_name_input,file_mame_arh);
		fclose(fp_input);
		fclose(fp_101);
		fclose(fp_result);

		return 0;
	}
	else if(strcmp(argv[1],"-d")==0)	//запускаем декомпрессор
	{
		int i=0;
		int ch;							//читаемый символ из файла (текущий)
		char signature[14];				//массив для хранения нашей подписи "Compresing!!!"
		char ch_count;					//количество уникальных символов
		char ch_tail;					//хвост
		char buf[8];					//массив для хранения битов 

		fp_input=fopen("war_peace.txt.arh","rb");	//открываем архив, читаем
		
		/* ПРОВЕРКА АРХИВА НА ПОДЛИННОСТЬ НАШЕЙ ПОДПИСЬЮ
		char signature[13]="Compresing!!!";			//наша подпись в архиве
		char buf1[14]={0};
		fread(&buf1,sizeof(char),13,fp_input);		//читаем архив и заносим подпись в буфер
		//printf("%s\n",buf);
		if(strcmp(buf1,signature)==0)//проверяем соответствует ли подпись в архиве - подписи нашего архиватора
		{
			printf("\nThis file doesn't unzip our archivation!!!\n");
			return 0; //если подпись не соответствует выводим сообщение и выходим из программы
		}
		*/


		//читаем заголовок в архиве
		fread(&signature,sizeof(char),13,fp_input);	//прочитали нашу подпись "Compresing!!!"
		fread(&ch_count,sizeof(char),1,fp_input);	//считали кол-во уникальных символов
		
		unique_symbol=(int)ch_count;				//перевод из char в int
		
		for(i=0;i<unique_symbol;i++) //считали таблицу: код символа-частота
		{
			fread(&simbols[i].ch,sizeof(char),1,fp_input);
			fread(&simbols[i].freq,sizeof(float),1,fp_input);
		}
		
		fread(&ch_tail,sizeof(char),1,fp_input);	//вычисляем хвост
		tail_length=(int)ch_tail;					//перевод из char в int

		fp_101=fopen("temp.101","wb");				//читаем промежуточный бинарный файл

		char ch_decoder;							//распаковываем
		ch_decoder=fgetc(fp_input);
		
		while((ch=fgetc(fp_input))!=EOF)
		{
			code.ch=ch_decoder;
			fputc(code.byte.b1+'0',fp_101);
			fputc(code.byte.b2+'0',fp_101);
			fputc(code.byte.b3+'0',fp_101);
			fputc(code.byte.b4+'0',fp_101);
			fputc(code.byte.b5+'0',fp_101);
			fputc(code.byte.b6+'0',fp_101);
			fputc(code.byte.b7+'0',fp_101);
			fputc(code.byte.b8+'0',fp_101);
			ch_decoder=ch;
		}

		code.ch=ch_decoder;
		buf[0]=code.byte.b1+'0';
		buf[1]=code.byte.b2+'0';
		buf[2]=code.byte.b3+'0';
		buf[3]=code.byte.b4+'0';
		buf[4]=code.byte.b5+'0';
		buf[5]=code.byte.b6+'0';
		buf[6]=code.byte.b7+'0';
		buf[7]=code.byte.b8+'0';
		
		for(i=0;i<tail_length;i++)
			fputc(buf[i],fp_101);

		//построение таблицы символов
		i=0;
		while(simbols[i].ch!='\0')				//установка указателей на структуру
		{
			psym[i]=&simbols[i];
			i++;
		}
		
		SYM *root=buildTree(psym,unique_symbol);//вызов функции создания дерева Хаффмана
		makeCodes(root);//вызов функции получения кода
		
		//читаем битовый файл и раскодируем его, записываем раскодированный файл
		fp_101=fopen("temp.101","rb");
		fp_result=fopen("war_peace.txt","wb");
		
		while(!feof(fp_101))							//функция раскодирования 
			fputc(search_code(fp_101,root),fp_result);
		
		printf("File \"war_peace.txt.arh\" unpack to \"war_peace.txt\"!\n");
		
		fclose(fp_input);
		fclose(fp_101);
		fclose(fp_result);

		return 0;
	}
	else//ничего не запускаем, т.к. неправильно указаны параметры в командной строке
	{
		printf("\nError. Invalid option -c  -d!!!\n");
	}

	return 0;
}