/* Практикум 3. Задача 9.
Написать программу, определяющую в строке символьную последовательность
максимальной длины
Замечание:
Для строки AABCCCDDEEEEF выводится 4 и EEEE
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 256

int main( )
{
	char string[N];
	int i,count=0,max=0,startWord=0;

	puts("Enter a string:");
	fgets(string,N,stdin);

	for(i=0;string[i];i++)
		if(string[i]!=' '&&string[i]==string[i+1])
			count++;
		else
		{
			if(count > max)
			{
				max=count;
				startWord=i-count;
			}
			count=0;
		}

	printf("%d - ",max+1);

	for(i=startWord; i<startWord+max+1; i++)
		putchar(string[i]);

	printf("\n");

	return 0;
}
