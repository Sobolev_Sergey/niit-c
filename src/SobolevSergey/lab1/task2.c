/* Практикум 1. Задача 2.
Написать программу, которая запрашивает текущее время в фор-
мате ЧЧ:ММ:СС, а затем выводит приветствие в зависимости от
указанного времени ("Доброе утро "Добрый день"и т.д.)
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void cleanIn() {
    char c;
    do 
	{
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int main()
{
    int h, m, s;
    while (1) 
	{
        puts("Enter your current time in the following format (HH:MM:SS):");
        if (scanf("%d:%d:%d", &h, &m, &s) == 3 && 
            (h >= 0 && h < 24) && 
            (m >= 0 && m < 60) && 
            (s >= 0 && s < 60))
            break;
        else
        {
            puts("Input error!");
            cleanIn();
        }
    }
    if (h >= 6 && h < 12)
        puts("Good morning!\n");
    else if (h >= 12 && h < 18)
        puts("Good afternoon!\n");
    else if (h >= 18 && h < 24)
        puts("Good evening!\n");
    else if (h >= 0 && h < 6)
        puts("Good night!\n");
    return 0;
}