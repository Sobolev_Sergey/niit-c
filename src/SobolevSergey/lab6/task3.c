/* Практикум 6. Задача 3.
Написать программу, которая переводит введённое пользователем
целое число в строку с использованием рекурсии и без каких-либо
библиотечных функций преобразования
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

void reverse_atoi(int n)
{
	if(n < 0) 
	{
		putchar('-');
		n=-n;
	}
	
	if(n/10)
		reverse_atoi(n/10);
	
	putchar(n%10+'0');
}

int main( )
{
	int n;
	
	printf("Please enter an integer number: ");
	scanf("%d", &n);
	printf("A number converted to a string: ");
	reverse_atoi(n);
	putchar('\n');
	
	return 0;
}