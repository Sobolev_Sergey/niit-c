/* Практикум 2. Задача 2. 
Написать программу ”Угадай число”. Программа задумывает число в диапа-
зоне от 1 до 100 и пользователь должен угадать его за наименьшее количество
попыток.
Замечание:
Пользователь вводит число, а программа подсказывает ему: ”больше”, ”мень-
ше”, ”угадал!”
*/

#define _CRT_SECURE_NO_WARNINGS
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

int main() 
{
	srand(time(NULL));
	int n,m=0;
	n=rand()%100;

	while(1)
	{
		printf("Enter the number: ");
		scanf("%d",&m);
		if(m < n) 
			printf("More!\n"); // больше
		else if(m > n)
			printf("Less!\n"); // меньше
		else
		{
			printf("Yes! The correct answer: %d\n",n);
			break;
		}
	}
	
	return 0;
}