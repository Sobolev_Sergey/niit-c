/* Практикум 2. Задача 5. 
Написать программу, которая выводит на экран 10 паролей, сгенерированных
случайным образом из латинских букв и цифр, причём буквы должны быть
как в нижнем, так и в верхнем регистрах. Длина пароля - 8 символов.
Замечание:
Пример сгенерированного пароля: Nh1ku83k*/

//********************* 1й  вариант решения    ************************
#include <stdio.h>                      
#include <stdlib.h>
#include <time.h>
#define N 8

char random_symbol()
{
	switch(rand()%3)
	{
	case 0:
		return rand()%26+'a';
	case 1:
		return rand()%26+'A';
	case 2:
		return rand()%10+'0';
	}
}

int main()
{
	int i,h;
	char arr[N];
	srand(time(NULL));
	
	for(h=0; h<10; h++)
	{
		for(i=0; i<N; i++)
		{
			arr[i]=random_symbol();
			putchar(arr[i]);
		}
		
		putchar('\n');
	}
	
	return 0;
}

/*
//********************* 2й  вариант решения    ************************
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#define N 8

int main(void)
{
	int h, i, j;
	char s[]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	char pass[N];
	
	srand(time(NULL));

	for(h=0; h<10; h++)
	{
		for(i=0,j=0; j<8; j++)
		{
			i=rand()%62;
			pass[j]=s[i];
			printf("%c",pass[j]);
		}
		
		putchar('\n');
	}

	return 0;
}
*/