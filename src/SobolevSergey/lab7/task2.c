/* Практикум 7. Задача 2.
Написать программу, которая анализирует исходный код файла на
языке Си и выводит таблицу встречаемости ключевых слов языка.
Ключевые слова хранятся в отдельном файле.

Замечание:
Работа программы осуществляется в несколько этапов:
(a) Вручную создается файл с ключевыми словами языка С.
(b) Открывается файл с ключевыми словами и строится бинарное дерево с
упорядоченными данными для всех слов.
(c) Открывается анализируемый файл с текстом программы и читается по
словами (или символам);
(d) Полученные строки (слова) ищутся в бинарном дереве;
(e) Если слово совпадает с хранящимся в дереве, увеличиваем счётчик встре-
чаемости данного слова;

Замечание:
Программа должна состоять из следующих функций:
(a) chomp - удаление символа конца строки.
(b) makeTree - создание дерева ключевых слов.(c) searchTree - поиск в дереве текущей комбинации символов.
(d) printTree - печать списка ключевых слов и их количества.
(e) main - главная функция программы.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 256
int max=0;

struct NODE
{
	int count;
	char word[SIZE];
	struct NODE *left;
	struct NODE *right;
};

struct NODE * makeTree(struct NODE *root,char *word)
{
	if(root==NULL)
	{
		struct NODE *temp=(struct NODE*)malloc(sizeof(struct NODE));
		temp->count=0;
		strcpy(temp->word,word);
		temp->left=temp->right=NULL;
		return temp;
	}
	else if(strcmp(root->word,word)>0)
	{
		root->left=makeTree(root->left,word);
		return root;
	}
	else if(strcmp(root->word,word)<0)
	{
		root->right=makeTree(root->right,word);
		return root;
	}
	else
	{
		root->count++;
		return root;
	}
}

void searchTree(struct NODE *root,char *buf)
{
	if(root)
	{
		searchTree(root->left,buf);
		if(strcmp(root->word,buf)==0)
			root->count++;
		searchTree(root->right,buf);
	}
}

void search_max(struct NODE *root)
{
	if(root)
	{
		search_max(root->left);
		if(max<root->count)
			max=root->count;
		search_max(root->right);
	}
}

void printTree_sort(struct NODE *root)
{
	if(root)
	{
		printTree_sort(root->left);
		if(max==root->count)
			printf("%s - %d\n",root->word,root->count);
		printTree_sort(root->right);
	}
}

int main(int argc,char *argv[])
{
	struct NODE *root=NULL;
	FILE *fp=fopen("keywords.txt","rt");

	char buf[SIZE];
	while(!feof(fp))
	{
		fscanf(fp,"%s",buf);
		root=makeTree(root,buf);
	}
	fclose(fp);

	fp=fopen(argv[1],"rt");
	while(!feof(fp))
	{
		fscanf(fp,"%s",buf);
		searchTree(root,buf);
	}
	fclose(fp);

	search_max(root);
	
	while(max>=0)
	{
		printTree_sort(root);
		max--;
	}

	return 0;
}