/* Практикум 5. Задача 4.
Написать программу, которая читает построчно текстовый файл и
переставляет случайно слова в каждой строке
Замечание:
Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки вызывается функция, разработанная в рамках задачи
1.
*/

#define _CRT_SECURE_NO_WARNINGS
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>

#define M 256

int getWords(char* string,char** startWord,int len);
int randomIndex(int* index,int countWord);
void printWord(char *string,char** startWord,int* index,int countWord);

int main( )
{
	int len=0;
	char string[M];
	char *startWord[M]={0};
	int index[M/2]={0};
	int countWord;
	srand(time(NULL));
	
	FILE* fp;

	fp=fopen("input.txt","rt");
	if(fp==NULL)
	{
		perror("File:");
		return 1;
	}
	srand(time(NULL));

	while(fgets(string,sizeof(string),fp))
	{
		len=strlen(string)-1;
		string[strlen(string)-1]=0;
		
		countWord=getWords(string,startWord,len);
		randomIndex(index,countWord);
		printWord(string,startWord,index,countWord);
	}

	printf("\n");
	fclose(fp);
	return 0;
}