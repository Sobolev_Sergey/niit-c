/*
 Написать программу, имитирующую работу высотомера бомбы. Бом-
ба падает с высоты H, которая задается пользователем. В любой
момент времени можно узнать пройденное расстояние по формуле
L =
gt2
2
, где g = 9.81 m
c
2
.
Высотомер бомбы срабатывает раз в секунду и выводит на терми-
нал текущее значение высоты над поверхностью земли h.
Замечание:
t=00 c h=5000.0 м
t=01 c h=4995.4 м
...
t=31с h=0234.8 м
BABAH!!!
*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define G 9.81
#define ERROR "Wrong input!\n"

void clearBoof(void)
{
    char bufclear = 0;
    
    while(bufclear != '\n')
        scanf("%c", &bufclear);//clear buffer
    bufclear = 0;
}

int isRestart(void)
{
    char restart = 0;
    
    while(1)
    {
        printf("Restart? (y/n):");
        if((scanf("%c", &restart) != 1) || ((restart != 'y') && (restart != 'n')))
        {
            printf(ERROR);
            clearBoof();
            continue;
        }

        //check symbols after char
        if(getchar() != '\n')
        {
            printf(ERROR);
            clearBoof();
            continue;
        }
        //clearBoof();
        
        if(restart == 'y')
            return 1;
        else
            return 0;
    }
}

    int inputInt(char hi[], int min, int max)
    {
        int i = 0;

        while(1)
        {
            printf("%s", hi);
            if((scanf("%d", &i) != 1) || i < min || i > max)
            {
                printf(ERROR);
                clearBoof();
                continue;
            }

        if(getchar() != '\n')
        {
            printf(ERROR);
            clearBoof();
            continue;
        }

        //clearBoof();
        break;
    }
    return i;
}

int main()
{

    float altitude, l;
    int time;
    time_t wait = 0;

    do
    {
        time = 0, l = 0.0;
        altitude = inputInt("Input Altitude: ", 1, 100000);

        while(altitude > l)
        {
            printf("t = %d c \t h = %.1f m\n", time, altitude - l);
            time++;
            l = (G * time * time / 2.0);
            // sleep 1 sec
            wait = clock() + 1 * CLOCKS_PER_SEC ;
            while(clock() < wait);
        }
        
        printf("BABAH!!!\n");
    
    }
    while(isRestart());

    return 0;
}