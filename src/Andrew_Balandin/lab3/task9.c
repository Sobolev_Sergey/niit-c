/*
Написать программу, определяющую в строке символьную последовательность
максимальной длины
Замечание:
Для строки AABCCCDDEEEEF выводится 4 и EEEE
 */

#include <stdio.h>

#define N 256

int main()
{
    int     i = 0,
            nsymb = 0,
            nmaxsymb = 0;
    
    char    prevch = 0,
            maxch = 0;
    
    char    str[N];
    
    printf("Input your string: ");
    fgets(str, N, stdin);
    
    prevch = str[0];
    for(i = 0; str[i] != '\n'; i++)
    {
        if(str[i] == prevch)
            nsymb++;
        if(str[i] != prevch)
        {
            prevch = str[i];
            nsymb = 1;
        }
        if(nsymb > nmaxsymb)
        {
            nmaxsymb = nsymb;
            maxch = str[i];
        }
    }
    
    printf("%d ", nmaxsymb);
    for(i = 0; i < nmaxsymb; i++)
        putchar(maxch);
    putchar('\n');
    
    return 0;
}