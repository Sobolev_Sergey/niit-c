#include <stdio.h>

int main(void)
{
    char t_day[][30]={"Good morning", "Good afternoon", "Good evening", "Good night"};
   
    int hr, mt, sc;
    
    printf("Enter the time(hh:mm:ss): ");
   
    scanf("%2i %2i %2i", &hr, &mt, &sc);
    
   if (hr > 4 && hr < 12)
    {
        printf ("%s\n", t_day[0]);
    }    
    else if((hr == 12 && mt > 0) || (hr >= 13 && hr < 17))
    {
        printf ("%s\n", t_day[1]);
    }    
    else if ((hr >= 17 && mt > 0) || hr > 18)
    {
        printf ("%s\n", t_day[2]);
    }
    else if (hr > 0 && hr < 4)
    {
        printf ("%s\n", t_day[3]);
    }
    
    return 0;
}